import os

import numpy as np
import tensorflow as tf


class ReplayBuffer():
    def __init__(self, max_size, num_actions, num_states, epsilon=0.0001, alpha=0.6, beta=0.4, prioritized_replay=False, load_saved_buffer=True, buffer_name='saved_buffer'):
        self.state_buffer = np.zeros((max_size, num_states), dtype=np.float32)
        self.action_buffer = np.zeros((max_size, num_actions), dtype=np.float32)
        self.reward_buffer = np.zeros((max_size), dtype=np.float32)
        self.next_state_buffer = np.zeros((max_size, num_states), dtype=np.float32)
        self.priorities = np.zeros((max_size), dtype=np.float32)
        self.not_done_buffer = np.zeros((max_size), dtype=np.float32)

        self.buffer_name = buffer_name
        self.pointer = 0
        self.size = 0
        self.max_size = max_size
        self.prioritized_replay = prioritized_replay
        self.epsilon = epsilon  # small constant preventing that error is zero
        self.alpha = alpha  # grade of priorization (0: uniform priorization)
        self.beta = beta  # grade of importance sampling, prevents bias
        self.beta_inc = 0.001  # beta increment, used for beta annealing towards 1

        if load_saved_buffer:
            self.load_buffer(dir_name=self.buffer_name)

    def save(self, states, actions, rewards, next_states, dones):
        """
        Save experience to buffer.
        """
        experience = (states, actions, rewards, next_states, dones)

        # if buffer is empty, set max_prio to 1
        max_prio = np.max(self.priorities) if not np.all(self.priorities == 0) else 1.0

        for state, action, reward, next_state, done in zip(*experience):
            self.state_buffer[self.pointer] = state
            self.action_buffer[self.pointer] = action
            self.reward_buffer[self.pointer] = reward
            self.next_state_buffer[self.pointer] = next_state
            self.priorities[self.pointer] = max_prio
            self.not_done_buffer[self.pointer] = 1. - done

            self.pointer = (self.pointer + 1) % self.max_size
            self.size = min(self.size + 1, self.max_size)

    def sample_batch(self, batch_size):
        """
        Return random experience sample.
        """
        self.beta = min([1, self.beta + self.beta_inc])

        if self.prioritized_replay:
            sampling_probabilities = self.priorities[:self.size] ** self.alpha
            sampling_probabilities /= np.sum(sampling_probabilities)
            indices = np.random.choice(self.size, size=batch_size, p=sampling_probabilities)
            weights = (self.size * sampling_probabilities[indices]) ** (-self.beta)
            weights /= weights.max()
        else:
            indices = np.random.randint(0, self.size, size=batch_size)
            weights = np.ones(batch_size, dtype=np.float32)

        states = tf.convert_to_tensor(self.state_buffer[indices])

        actions = tf.convert_to_tensor(self.action_buffer[indices])

        rewards = tf.convert_to_tensor(self.reward_buffer[indices])
        rewards = tf.reshape(rewards, [batch_size, -1])

        next_states = tf.convert_to_tensor(self.next_state_buffer[indices])

        weights = tf.convert_to_tensor(weights)
        weights = tf.cast(weights, tf.float32)
        weights = tf.reshape(weights, [batch_size, -1])

        not_done = tf.convert_to_tensor(self.not_done_buffer[indices])
        not_done = tf.reshape(not_done, [batch_size, -1])

        return (
            states,
            actions,
            rewards,
            next_states,
            indices,
            weights,
            not_done
        )

    def update(self, priority, indices):
        """
        Update the priority values.
        """
        for p, i in zip(priority, indices):
            self.priorities[i] = np.abs(p) + self.epsilon

    def store_buffer(self, dir_name='saved_buffer'):
        """
        Save the replay buffer and its content.
        """
        np.save(f'./{dir_name}/state_buffer.npy', self.state_buffer)
        np.save(f'./{dir_name}/action_buffer.npy', self.action_buffer)
        np.save(f'./{dir_name}/reward_buffer.npy', self.reward_buffer)
        np.save(f'./{dir_name}/next_state_buffer.npy', self.next_state_buffer)
        np.save(f'./{dir_name}/priorities.npy', self.priorities)

    def load_buffer(self, dir_name='saved_buffer'):
        """
        Load the replay buffer.
        """
        os.makedirs(f'./{dir_name}/', exist_ok=True)

        self.state_buffer = np.load(f'./{dir_name}/state_buffer.npy')
        self.action_buffer = np.load(f'./{dir_name}/action_buffer.npy')
        self.reward_buffer = np.load(f'./{dir_name}/reward_buffer.npy')
        self.next_state_buffer = np.load(f'./{dir_name}/next_state_buffer.npy')
        self.priorities = np.load(f'./{dir_name}/priorities.npy')
        self.size = self.reward_buffer.shape[0]
        print(dir_name)
        print(self.size)
        print(self.state_buffer.shape)
