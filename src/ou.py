"""
Ornstein–Uhlenbeck process.
"""

import numpy as np

class OUNoise():
    def __init__(self, params, mu=0.0, theta=0.15, max_sigma=0.3, min_sigma=None):
        self.mu = mu
        self.theta = theta
        self.sigma = max_sigma
        self.max_sigma = max_sigma
        self.min_sigma = min_sigma
        self.decay_period = params['decay_period']
        self.action_dim = 2
        self.low = -params['action_max']
        self.high = params['action_max']
        self.nr_agents = params['num_agents']
        self.nr_actions = params['num_actions']
        self.reset()

    def reset(self):
        self.state = np.ones(self.action_dim) * self.mu

    def evolve_states(self):
        states = []
        for i in range(self.nr_agents*self.nr_actions):
            x = self.state
            dx = self.theta * (self.mu - x) + self.sigma * np.random.randn(self.action_dim)
            self.state = x + dx
            states.append(self.state)
        new_states = np.resize(np.array(states), (self.nr_agents, self.nr_actions))
        return new_states

    def policy(self, actions, t=0):
        ou_states = self.evolve_states()
        if self.min_sigma is None:
            self.min_sigma = self.max_sigma
        self.sigma -= self.sigma * min(1.0, t / self.decay_period)

        next_actions = (actions + ou_states).clip(self.low, self.high)
        return next_actions
