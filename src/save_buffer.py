"""
Create a prefilled buffer sampled from random actions.
Stored in '/TD3/saved_buffer/
"""
import os

import numpy as np
from mlagents_envs.registry import default_registry

from replay_buffer import ReplayBuffer


env = default_registry['WormDynamicTarget'].make(no_graphics=True, worker_id=1)
env.reset()
behavior_name = list(env.behavior_specs.keys())[0]
decision_steps, terminal_steps = env.get_steps(behavior_name)
states = decision_steps.obs[0]

BUFFER_NAME = 'saved_buffer_500k_dynamic'
NUM_ACTIONS = env.behavior_specs[behavior_name].action_shape
NUM_STATES = env.behavior_specs[behavior_name].observation_shapes[0][0]
N_AGENTS = 10
TOTAL_STEPS = 100 * 500
episode_length = 0

os.makedirs(BUFFER_NAME, exist_ok=True)

buffer = ReplayBuffer(max_size=TOTAL_STEPS * N_AGENTS, num_actions=NUM_ACTIONS, num_states=NUM_STATES, prioritized_replay=True, load_saved_buffer=False)

for t in range(1, TOTAL_STEPS + 1):
    actions = np.random.randn(N_AGENTS, NUM_ACTIONS)

    # send actions to the environment
    env.set_actions(behavior_name, actions)

    # take one step
    env.step()

    # receive observations from environment
    decision_steps, terminal_steps = env.get_steps(behavior_name)
    next_states = decision_steps.obs[0]
    rewards = decision_steps.reward
    done = [0. for i in range(N_AGENTS)]

    # save experience to replay buffer
    buffer.save(states, actions, rewards, next_states, done)

    # update observation
    states = next_states

    episode_length += 1

    # reset the environment if terminal step was reached
    if len(terminal_steps) > 0 or (episode_length == 500):
        env.reset()
        print(t)
        episode_length = 0

buffer.store_buffer(dir_name=BUFFER_NAME)
