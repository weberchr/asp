import argparse
import json
import os

import numpy as np
from mlagents_envs.registry import default_registry, UnityEnvRegistry

from td3 import TD3Agent
from ddpg import DDPGAgent
from ou import OUNoise
from utils import plot_loss, plot_reward

# parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-o', '--optuna', action='store_true', default=False, help='hyperparameter tuning with optuna')
parser.add_argument('-g', '--no-graphics', action='store_true', default=False, help='deactivate unity graphics')
parser.add_argument('-w', '--worker-id', type=int, default=1, help='unity environment worker id')
parser.add_argument('-p', '--parameter', type=str, default='default', help='give additional parameters that override default parameters. Parameters must exist as json file in settings')


args = parser.parse_args()

# parameter file existence check
if args.parameter is not None:
    if os.path.isfile('settings/' + args.parameter + '.json'):
        print('using additional parameter settings with', 'settings/' + args.parameter + '.json')
        MODEL_PATH = f'./Modelle/{args.parameter}/'
    else:
        print('no such parameter file')
        quit()

# create model directory
os.makedirs(MODEL_PATH, exist_ok=True)

# instantiate environment
env = default_registry['WormDynamicTarget'].make(no_graphics=args.no_graphics, worker_id=args.worker_id)

# to load Worm2
# registry = UnityEnvRegistry()
# registry.register_from_yaml('../Agenten/WormStaticTarget2.yaml')
# env = registry['WormStatic2'].make(no_graphics=args.no_graphics, worker_id=args.worker_id)


def load_optuna_params(trial, params):
    d = json.load(open('settings/optuna.json', 'r'))
    for p in d:
        for k, v in d[p].items():
            if p == 'float':
                params[k] = trial.suggest_float(k, v[0], v[1])
            if p == 'int':
                params[k] = trial.suggest_int(k, v[0], v[1])
            if p == 'categorical':
                params[k] = trial.suggest_categorical(k, v)
    return params


def main(trial=None):
    env.reset()
    behavior_name = list(env.behavior_specs.keys())[0]
    decision_steps, terminal_steps = env.get_steps(behavior_name)
    states = decision_steps.obs[0]

    # load training parameters
    params = {}
    param_dict = json.load(open('settings/default.json', 'r'))
    for p in param_dict:
        params[p] = param_dict[p]

    if args.parameter != None and os.path.isfile('settings/' + args.parameter + '.json'):
        param_dict = json.load(open('settings/' + args.parameter + '.json', 'r'))
        for p in param_dict:
            params[p] = param_dict[p]

    NUM_EPISODES = params['num_episodes']
    MAX_EPISODE_LEN = params['max_episode_len']
    WARMUP_PHASE = params['warmup_phase']  # take random actions for this many steps
    N_AGENTS = params['num_agents']
    params['num_actions'] = env.behavior_specs[behavior_name].action_shape
    params['num_states'] = env.behavior_specs[behavior_name].observation_shapes[0][0]

    print(params)

    if trial is not None:
        params = load_optuna_params(trial, params)
        NUM_EPISODES = int(NUM_EPISODES / 10)
        MAX_EPISODE_LEN = int(MAX_EPISODE_LEN / 10)
        WARMUP_PHASE = int(WARMUP_PHASE / 10)

    TOTAL_STEPS = NUM_EPISODES * MAX_EPISODE_LEN

    if params['decay_period'] == -1:
        params['decay_period'] = MAX_EPISODE_LEN * NUM_EPISODES - WARMUP_PHASE

    if params['agent'] == 'TD3':
        agent = TD3Agent(params)
    elif params['agent'] == 'DDPG':
        agent = DDPGAgent(params)
    else:
        print('specify an agent in the parameter file')
        quit()

    noise = OUNoise(params)

    mean_reward = []
    min_reward = []
    max_reward = []
    rolling_mean_reward = []

    actor_loss = []
    critic_loss = []
    rolling_mean_actor_loss = []
    rolling_mean_critic_loss = []

    episode_length = 0
    episode_reward = np.zeros(N_AGENTS)

    for t in range(1, TOTAL_STEPS + 1):
        episode_length += 1

        if t > WARMUP_PHASE:
            actions = agent.policy(states)
            if params['Noisetype'] == 'OU':
                actions = noise.policy(actions, t - WARMUP_PHASE)
            elif params['Noisetype'] == 'Gauß':
                actions += params['noise_scale'] * np.random.randn(N_AGENTS, params['num_actions'])
                actions = np.clip(actions, -1, 1)
        else:
            actions = np.random.randn(N_AGENTS, params['num_actions'])

        # send actions to the environment
        env.set_actions(behavior_name, actions)

        # take one step
        env.step()

        # receive observations from environment
        decision_steps, terminal_steps = env.get_steps(behavior_name)
        next_states = decision_steps.obs[0]
        rewards = decision_steps.reward

        done = [0. for i in range(N_AGENTS)]

        for i in terminal_steps.agent_id:
            done[i] = 1.

        # perform one training step
        agent.update(t)

        # plot loss
        actor_loss.append(agent.actor_loss)
        critic_loss.append(agent.critic_loss)
        rolling_mean_actor_loss.append(np.mean(actor_loss[-500:]))
        rolling_mean_critic_loss.append(np.mean(critic_loss[-500:]))
        plot_loss(rolling_mean_actor_loss, rolling_mean_critic_loss, MODEL_PATH)

        # reset the environment if terminal step was reached
        if len(terminal_steps) > 0 or (episode_length == MAX_EPISODE_LEN):
            states_ = []
            actions_ = []
            rewards_ = terminal_steps.reward
            next_states_ = terminal_steps.obs[0]
            done_ = [1. for _ in range(len(terminal_steps))]

            for i in terminal_steps.agent_id:
                states_ = states[i]
                actions_ = actions[i]

            agent.replay_buffer.save(states_, actions_, rewards_, next_states_, done_)

            env.reset()
            decision_steps, _ = env.get_steps(behavior_name)
            states = decision_steps.obs[0]

            mean_reward.append(np.mean(episode_reward))
            min_reward.append(np.min(episode_reward))
            max_reward.append(np.max(episode_reward))
            rolling_mean_reward.append(np.mean(mean_reward[-100:]))
            episode_reward = np.zeros(N_AGENTS)

            print(f'{t}\t{episode_length}\t{mean_reward[-1]}\t{min_reward[-1]}\t{max_reward[-1]}\t{rolling_mean_reward[-1]}')
            plot_reward((mean_reward, min_reward, max_reward, rolling_mean_reward), MODEL_PATH)

            episode_length = 0
            continue

        # save experience to replay buffer
        agent.replay_buffer.save(states, actions, rewards, next_states, done)

        # update observation
        states = next_states
        episode_reward += rewards

        # save model architecture and weights
        if (t % params['model_saving_freq']) == 0:
            agent.save_model(MODEL_PATH)

    if trial is not None:
        results[np.mean(mean_reward)] = params
        return np.mean(mean_reward)


if __name__ == '__main__':
    if args.optuna:
        import optuna
        results = {}
        study = optuna.create_study(direction='maximize')
        study.optimize(main, n_trials=100)
        json.dump(study.best_params, open('settings/optimized_params.json', 'w'))
        json.dump(results, open('settings/full_params_log.json', 'w'))
    else:
        main()

    env.close()
