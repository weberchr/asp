import json


class BasicAgent:

    def __init__(self):
        self.actor = self._build_actor_network()
        self.critic = self._build_critic_network()

    def _build_actor_network(self):
        pass

    def _build_critic_network(self):
        pass    

    def save_model(self, path, overwrite=True):
        """
        Save actor and critic weights and architecture.
        """
        self.actor.save_weights(path + 'actor.h5', overwrite=overwrite)
        self.critic.save_weights(path + 'critic.h5', overwrite=overwrite)

        with open(path + 'actor.json', 'w') as f:
            json.dump(self.actor.to_json(), f)

        with open(path + 'critic.json', 'w') as f:
            json.dump(self.critic.to_json(), f)

    def _load_model(self, name):
        self.actor.load_weights(f'./Modelle/{name}/actor.h5')
        self.critic.load_weights(f'./Modelle/{name}/critic.h5')
        print(f'model {name} successfully loaded.')
