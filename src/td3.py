import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.backend import minimum
from tensorflow.keras.layers import Concatenate, Dense, Input
from tensorflow.keras.optimizers import Adam

from basic_agent import BasicAgent
from replay_buffer import ReplayBuffer


class TD3Agent(BasicAgent):
    def __init__(self, params):
        self.gamma = params['gamma']
        self.polyak = params['polyak']
        self.batch_size = params['batch_size']
        self.num_actions = params['num_actions']
        self.num_states = params['num_states']
        self.hidden_size = params['hidden_size']
        self.noise_scale = params['noise_scale']
        self.policy_freq = params['policy_freq']
        self.noise_clip = params['noise_clip']
        self.batch_size = params['batch_size']
        self.action_max = params['action_max']
        self.prioritized_replay = params['prioritized_replay']
        self.noise_at_update = params['noise_at_update']

        self.replay_buffer = ReplayBuffer(
            params['buffer_size'], params['num_actions'], params['num_states'], self.prioritized_replay, buffer_name=params['buffer_name']
        )

        self.actor = self._build_actor_network()
        self.actor_target = self._build_actor_network()
        self.critic = self._build_critic_network()
        self.critic_target = self._build_critic_network()

        self.actor_optimizer = Adam(learning_rate=params['actor_learning_rate'])
        self.critic_optimizer = Adam(learning_rate=params['critic_learning_rate'])

        if params['model_name']:
            self._load_model(params['model_name'])

        self.actor_target.set_weights(self.actor.get_weights())
        self.critic_target.set_weights(self.critic.get_weights())

        self.actor.summary()
        self.critic.summary()

        self.actor_loss = 0
        self.critic_loss = 0

    def _build_actor_network(self):
        """
        Build the actor network.
        """
        inputs = Input(shape=(self.num_states,))
        hidden = Dense(self.hidden_size, activation='relu')(inputs)
        hidden = Dense(self.hidden_size, activation='relu')(hidden)
        actor = Dense(self.num_actions, activation='tanh', name='actor')(hidden)
        return keras.Model(inputs=inputs, outputs=actor, name='actor')

    def _build_critic_network(self):
        """
        Build the critic network.
        """
        # input of both networks
        states_input = Input(shape=(self.num_states,))
        actions_input = Input(shape=(self.num_actions,))
        concat = Concatenate()([states_input, actions_input])

        # q1 architecture
        hidden1 = Dense(self.hidden_size, activation='relu')(concat)
        hidden1 = Dense(self.hidden_size, activation='relu')(hidden1)
        q1 = Dense(1, name='q1')(hidden1)

        # q2 architecture
        hidden2 = Dense(self.hidden_size, activation='relu')(concat)
        hidden2 = Dense(self.hidden_size, activation='relu')(hidden2)
        q2 = Dense(1, name='q2')(hidden2)

        return keras.Model(inputs=[states_input, actions_input], outputs=(q1, q2), name='critic')

    def _update_target_networks(self):
        """
        Update the weights of the target networks.
        """
        critic_weights = np.array(self.critic.weights)
        critic_target_weights = np.array(self.critic_target.weights)
        updated_weights = self.polyak * critic_target_weights + (1 - self.polyak) * critic_weights
        self.critic_target.set_weights(updated_weights)

        actor_weights = np.array(self.actor.weights)
        actor_target_weights = np.array(self.actor_target.weights)
        updated_weights = self.polyak * actor_target_weights + (1 - self.polyak) * actor_weights
        self.actor_target.set_weights(updated_weights)

    def policy(self, states):
        """
        Use the actor to generate actions for given states.
        """
        return self.actor.predict_on_batch(states)

    def update(self, timestep):
        """
        Train actor and critic networks.
        """
        states, actions, rewards, next_states, indices, weights, not_done = self.replay_buffer.sample_batch(self.batch_size)

        # critic update
        with tf.GradientTape(watch_accessed_variables=False) as tape:
            tape.watch(self.critic.trainable_variables)

            next_actions = self.actor_target(next_states)

            if self.noise_at_update:
                #add additional noise to actions and clip for smoothing
                noise = (self.noise_scale * np.random.randn(self.num_actions)).clip(-self.noise_clip, self.noise_clip)
                next_actions = tf.clip_by_value(next_actions + noise, -self.action_max, self.action_max)

            target_q1, target_q2 = self.critic_target([next_states, next_actions])
            target_q_values = minimum(target_q1, target_q2)
            target_q_values = rewards + not_done * self.gamma * target_q_values
            q1, q2 = self.critic([states, actions])

            critic_loss = weights * (tf.square(q1 - target_q_values) + tf.square(q2 - target_q_values))
            critic_loss_mean = tf.reduce_mean(critic_loss)

        self.critic_loss = critic_loss_mean.numpy()

        # gradient of critic loss
        gradients = tape.gradient(critic_loss_mean, self.critic.trainable_variables)

        # update critic weights
        self.critic_optimizer.apply_gradients(zip(gradients, self.critic.trainable_variables))

        # update the sampling priorities
        if self.prioritized_replay:
            self.replay_buffer.update(critic_loss, indices)

        # delayed actor updates
        if timestep % self.policy_freq == 0:
            with tf.GradientTape(watch_accessed_variables=False) as tape:
                tape.watch(self.actor.trainable_variables)

                actions = self.actor(states)
                q1, _ = self.critic([states, actions])
                actor_loss = - tf.reduce_mean(q1)

            self.actor_loss = actor_loss.numpy()

            # compute gradient of actor loss
            gradients = tape.gradient(actor_loss, self.actor.trainable_variables)

            # update actor weights
            self.actor_optimizer.apply_gradients(zip(gradients, self.actor.trainable_variables))

            # transfer weights to target networks
            self._update_target_networks()
