import matplotlib.pyplot as plt

def plot_reward(rewards, filepath):
    """
    Plot and save the reward over episodes.
    """
    plt.rcParams['figure.figsize'] = (30, 15)

    mean_rewards, min_rewards, max_rewards, rolling_mean = rewards

    plt.plot(mean_rewards, c='b', linewidth=.8, label='Mean')
    plt.plot(min_rewards, c='r', linewidth=.8, label='Min')
    plt.plot(max_rewards, c='g', linewidth=.8, label='Max')
    plt.plot(rolling_mean, c='k', linewidth=1, label='Rolling Mean 100')

    plt.xlabel('Episode')
    plt.ylabel('Reward')
    plt.legend(loc='upper left')
    plt.grid(linewidth=.5)
    plt.tight_layout()
    plt.savefig(filepath + 'reward.png')
    plt.close()


def plot_loss(rolling_mean_actor_loss, rolling_mean_critic_loss, filepath):
    """
    Plot and save the loss over episodes.
    """
    plt.rcParams['figure.figsize'] = (30, 15)

    plt.plot(rolling_mean_actor_loss, c='c', linewidth=1, label='Actor - Rolling Mean 100')
    plt.plot(rolling_mean_critic_loss, c='m', linewidth=1, label='Critic - Rolling Mean 100')

    plt.xlabel('Timestep')
    plt.ylabel('Loss')
    plt.legend(loc='upper left')
    plt.grid(linewidth=.5)
    plt.tight_layout()
    plt.savefig(filepath + 'loss.png')
    plt.close()
