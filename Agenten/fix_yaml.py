import os
from yaml import load, dump, Loader


root, src = os.path.split(os.path.dirname(__file__))
print(root)

linux = 'file:///' + root + '/Agenten/WurmLinuxBuild.zip'
mac = 'file:///' + root + '/Agenten/WurmMacBuild.zip'
win = 'file:///' + root + '/Agenten/WurmWinBuild.zip'


stream = open('WormStaticTarget2.yaml', 'r')
data = load(stream, Loader=Loader)

if os.path.isfile(linux):
    data['environments'][0]['WormStaticTarget2']['linux_url'] = linux
if os.path.isfile(mac):
    data['environments'][0]['WormStaticTarget2']['darwin_url'] = mac
if os.path.isfile(win):
    data['environments'][0]['WormStaticTarget2']['win_url'] = win

stream.close()

stream = open('WormStaticTarget2.yaml', 'w')
dump(data, stream)
stream.close()