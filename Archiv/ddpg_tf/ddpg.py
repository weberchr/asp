import numpy as np
import tensorflow as tf
import gym
from buffer import BasicBuffer_old, BasicBuffer_new

# actor is a policy network that takes the state as inputs and outputs the action
# critic takes state and action as input and outputs Q-value for the state-action pair



#the simplest NN

def SNN (input_shape, layer_sizes, hidden_activation = 'relu', output_activation = None):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Input(shape = input_shape))
    # hidden layers for both networks have relu activations
    for h in layer_sizes[:-1]:
        x = model.add(tf.keras.layers.Dense(units = h, activation='relu'))
    model.add(tf.keras.layers.Dense(units=layer_sizes[-1],activation = output_activation))

    return model

#generate critic and actor networks using input-shape and layer-size parameters

def ddpg(
        env_fn, #make the gym env with a given env_name
        seed = 0,
        buffer_maxlen = int(1e6),
        num_train_episodes = 100,
        batch_size =32,
        gamma = 0.99,
        decay = 0.99,
        critic_lr = 1e-3,
        actor_lr = 1e-3,
        action_noise = 0.0,
        start_steps = 1000,
        max_episode_length = 500):

    tf.random.set_seed(seed)
    np.random.seed(seed)

    env,test_env = env_fn(), env_fn()

    num_steps = 0

    rewards = []

    # actor and critic losses
    mu_losses = []
    q_losses = []



    #getting size of state space and action space
    num_states = env.observation_space.shape[0]
    num_actions = env.action_space.shape[0]
    action_max = env.action_space.high[0]


    #Network parameters
    X_shape = (num_states)
    QA_shape = (num_states + num_actions)
    hidden_sizes_ac = (1000,500,200)
    hidden_sizes_cr = (400,200)

    #Main network outputs
    #for actor mu 'tahn', for mapping continuous actions -1 to 1
    mu = SNN(X_shape,list(hidden_sizes_ac)+[num_actions],hidden_activation='relu',output_activation='tanh')
    q_mu = SNN(QA_shape,list(hidden_sizes_cr)+[1],hidden_activation='relu')

    #Target networks
    mu_target = SNN(X_shape,list(hidden_sizes_ac)+[num_actions],hidden_activation='relu',output_activation='tanh')
    q_mu_target = SNN(QA_shape,list(hidden_sizes_cr)+[1],hidden_activation='relu')

    #Copying weights in
    mu_target.set_weights(mu.get_weights())
    q_mu_target.set_weights(q_mu.get_weights())

    #Experience replay memory
    replay_buffer = BasicBuffer_new(size = buffer_maxlen, obs_dim = num_states, act_dim = num_actions)

    #Training each network separate
    mu_opt = tf.keras.optimizers.Adam(learning_rate=actor_lr)
    q_opt = tf.keras.optimizers.Adam(learning_rate=critic_lr)

    #clipping the values in an array
    def get_action(s,noise_scale):
        a = action_max * mu.predict(s.reshape(1,-1))[0]
        a += noise_scale * np.random.randn(num_actions)
        return np.clip(a,-action_max,action_max)



    for episode in range(num_train_episodes):
        #reset env
        s, episode_reward,episode_length,d = env.reset(),0,0, False

        while not (d or (episode_length == max_episode_length)):
            #using randomly sampled actions for the first start steps for encouraging exploration
            if num_steps > start_steps:
                a = get_action(s,action_noise)
            else:
                a = env.action_space.sample()

            num_steps +=1

            #step the env
            s2,r,d,_ = env.step(a)
            episode_reward += r
            episode_length += 1

            #ignoring the done signal if it comes from hitting time
            d_store = False if episode_length == max_episode_length else d

            #store experience to replay buffer
            replay_buffer.push(s,a,r,s2,d_store)

            #next_state is the current state on the next round
            s = s2

        # Perform the updates
        for _ in range(episode_length):
            X, A, R, X2, D = replay_buffer.sample(batch_size)
            X = np.asarray(X, dtype=np.float32)
            A = np.asarray(A, dtype=np.float32)
            R = np.asarray(R, dtype=np.float32)
            X2 = np.asarray(X2, dtype=np.float32)
            D = np.asarray(D, dtype=np.float32)
            Xten = tf.convert_to_tensor(X)

            # Actor optimization
            with tf.GradientTape() as tape2:
                Aprime = action_max * mu(X)
                temp = tf.keras.layers.concatenate([Xten, Aprime], axis=1)
                Q = q_mu(temp)
                mu_loss = -tf.reduce_mean(Q)
                grads_mu = tape2.gradient(mu_loss, mu.trainable_variables)
            mu_losses.append(mu_loss)
            mu_opt.apply_gradients(zip(grads_mu, mu.trainable_variables))

            # Critic Optimization
            with tf.GradientTape() as tape:
                next_a = action_max * mu_target(X2)
                temp = np.concatenate((X2, next_a), axis=1)
                q_target = R + gamma * (1 - D) * q_mu_target(temp)
                temp2 = np.concatenate((X, A), axis=1)
                qvals = q_mu(temp2)
                q_loss = tf.reduce_mean((qvals - q_target) ** 2)
                grads_q = tape.gradient(q_loss, q_mu.trainable_variables)
            q_opt.apply_gradients(zip(grads_q, q_mu.trainable_variables))
            q_losses.append(q_loss)

            ## Updating both netwokrs
            ## updating Critic network

            temp1 = np.array(q_mu_target.get_weights())
            temp2 = np.array(q_mu.get_weights())
            temp3 = decay * temp1 + (1 - decay) * temp2
            q_mu_target.set_weights(temp3)

            # updating Actor network
            temp1 = np.array(mu_target.get_weights())
            temp2 = np.array(mu.get_weights())
            temp3 = decay * temp1 + (1 - decay) * temp2
            mu_target.set_weights(temp3)

        print("Episode:", episode + 1, "Reward:", episode_reward)
        rewards.append(episode_reward)
        
    return (rewards, q_losses, mu_losses)













