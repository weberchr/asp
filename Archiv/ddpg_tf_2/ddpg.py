import json

import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Concatenate, Dense, Input
from tensorflow.keras.optimizers import Adam


class ReplayBuffer():
    def __init__(self, max_size, batch_size, num_actions, num_states):
        self.state_buffer = np.zeros((max_size, num_states), dtype=np.float32)
        self.action_buffer = np.zeros((max_size, num_actions), dtype=np.float32)
        self.reward_buffer = np.zeros((max_size), dtype=np.float32)
        self.next_state_buffer = np.zeros((max_size, num_states), dtype=np.float32)
        self.pointer = 0
        self.size = 0
        self.max_size = max_size
        self.batch_size = batch_size

    def save(self, states, actions, rewards, next_states):
        """
        Save experience to buffer.
        """
        experience = (states, actions, rewards, next_states)

        for state, action, reward, next_state in zip(*experience):
            self.state_buffer[self.pointer] = state
            self.action_buffer[self.pointer] = action
            self.reward_buffer[self.pointer] = reward
            self.next_state_buffer[self.pointer] = next_state
            self.pointer = (self.pointer + 1) % self.max_size
            self.size = min(self.size + 1, self.max_size)

    def sample_batch(self):
        """
        Return experience sample.
        """
        indices = np.random.randint(0, self.size, size=self.batch_size)
        return (
            self.state_buffer[indices],
            self.action_buffer[indices],
            self.reward_buffer[indices],
            self.next_state_buffer[indices],
        )


class DDPGAgent():
    def __init__(self, params):
        self.gamma = params['gamma']
        self.polyak = params['polyak']
        self.batch_size = params['batch_size']
        self.num_actions = params['num_actions']
        self.num_states = params['num_states']
        self.hidden_size = params['hidden_size']
        self.noise_scale = params['noise_scale']

        self.replay_buffer = ReplayBuffer(params['buffer_size'], params['batch_size'], params['num_actions'], params['num_states'])

        self.actor = self._build_actor_network()
        self.actor_target = self._build_actor_network()
        self.critic = self._build_critic_network()
        self.critic_target = self._build_critic_network()

        self.actor_optimizer = Adam(learning_rate=params['actor_learning_rate'])
        self.critic_optimizer = Adam(learning_rate=params['critic_learning_rate'])

        self.actor_target.set_weights(self.actor.get_weights())
        self.critic_target.set_weights(self.critic.get_weights())

    def _build_actor_network(self):
        """
        Build the actor network.
        """
        model = keras.Sequential()
        inputs = Input(shape=(self.num_states,))
        hidden = Dense(self.hidden_size, activation='relu')(inputs)
        hidden = Dense(self.hidden_size, activation='relu')(hidden)
        hidden = Dense(self.hidden_size, activation='relu')(hidden)
        hidden = Dense(self.hidden_size, activation='relu')(hidden)
        actor = Dense(self.num_actions, activation='tanh', name='actor')(hidden)
        return keras.Model(inputs=inputs, outputs=actor, name='actor')

    def _build_critic_network(self):
        """
        Build the critic network.
        """
        actions_input = Input(shape=(self.num_actions,))
        states_input = Input(shape=(self.num_states,))
        concat = Concatenate()([actions_input, states_input])
        hidden = Dense(self.hidden_size, activation='relu')(concat)
        hidden = Dense(self.hidden_size, activation='relu')(hidden)
        hidden = Dense(self.hidden_size, activation='relu')(hidden)
        critic = Dense(1, name='critic')(hidden)
        return keras.Model(inputs=[states_input, actions_input], outputs=critic, name='critic')

    def _update_target_networks(self):
        """
        Update the weights of the target networks.
        """
        critic_weights = np.array(self.critic.weights)
        critic_target_weights = np.array(self.critic_target.weights)
        updated_weights = self.polyak * critic_weights + (1 - self.polyak) * critic_target_weights
        self.critic_target.set_weights(updated_weights)

        actor_weights = np.array(self.actor.weights)
        actor_target_weights = np.array(self.actor_target.weights)
        updated_weights = self.polyak * actor_weights + (1 - self.polyak) * actor_target_weights
        self.actor_target.set_weights(updated_weights)

    def save_model(self, path, overwrite=True):
        """
        Save actor and critic weights and architecture.
        """
        self.actor.save_weights(f'{path}_actor.h5', overwrite=overwrite)
        self.critic.save_weights(f'{path}_critic.h5', overwrite=overwrite)

        with open(f'{path}_actor.json', 'w') as f:
            json.dump(self.actor.to_json(), f)

        with open(f'{path}_critic.json', 'w') as f:
            json.dump(self.critic.to_json(), f)

    def policy(self, states):
        """
        Use the actor to generate actions for given states.
        """
        actions = self.actor.predict_on_batch(states)
        actions += self.noise_scale * np.random.randn(self.num_actions)
        return actions

    def update(self):
        """
        Train actor and critic networks.
        """
        states, actions, rewards, next_states = self.replay_buffer.sample_batch()
        states = tf.convert_to_tensor(states)
        actions = tf.convert_to_tensor(actions)
        rewards = tf.convert_to_tensor(rewards)
        next_states = tf.convert_to_tensor(next_states)

        # critic update
        with tf.GradientTape() as critic_tape:
            next_actions = self.actor_target(next_states)
            target_q_values = self.critic_target([next_states, next_actions])
            critic_target = rewards + self.gamma * target_q_values
            q_values = self.critic([states, actions])
            critic_loss = tf.reduce_mean(tf.square(q_values - critic_target))

        # gradient of critic loss
        gradients = critic_tape.gradient(critic_loss, self.critic.trainable_variables)

        # update critic weights
        self.critic_optimizer.apply_gradients(zip(gradients, self.critic.trainable_variables))

        # actor update
        with tf.GradientTape() as actor_tape:
            actions = self.actor(states)
            q_values = self.critic([states, actions])
            actor_loss = - tf.reduce_mean(q_values)

        # compute gardient of actor loss
        gradients = actor_tape.gradient(actor_loss, self.actor.trainable_variables)

        # update actor weights
        self.actor_optimizer.apply_gradients(zip(gradients, self.actor.trainable_variables))

        # transfer weights to target networks
        # maybe do this every n steps?
        self._update_target_networks()
