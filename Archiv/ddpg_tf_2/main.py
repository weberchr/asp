"""
TODO
- set seed for env, tf and np
- Huber loss
- implement logger/tensorboard
- start steps: for first n steps, get random action instead by actor+noise
- check if environment loop is correct
"""

import numpy as np
import matplotlib.pyplot as plt
from mlagents_envs.registry import default_registry

from ddpg import DDPGAgent

SEED = 42
NUM_EPISODES = 2000
MAX_EPISODE_LEN = 2 
WARMUP_PHASE = 100
NAME = 'test_run'

env = default_registry['WormStaticTarget'].make()
env.reset()
behavior_name = list(env.behavior_specs.keys())[0]

params = {}
params['num_actions'] = env.behavior_specs[behavior_name].action_shape
params['num_states'] = env.behavior_specs[behavior_name].observation_shapes[0][0]
params['gamma'] = 0.99
params['polyak'] = 0.995
params['alpha'] = 0.001
params['buffer_size'] = 100000
params['hidden_size'] = 256
params['actor_learning_rate'] = 0.001
params['critic_learning_rate'] = 0.001
params['batch_size'] = 64
params['noise_scale'] = 0.1
params['seed'] = SEED

agent = DDPGAgent(params)

total_steps = NUM_EPISODES * MAX_EPISODE_LEN

env.reset()
decision_steps, terminal_steps = env.get_steps(behavior_name)

episode_reward = np.zeros(10)
total_rewards = []
episode_length = 0

for t in range(total_steps):
    states = decision_steps.obs[0]
    actions = agent.policy(states)

    env.set_actions(behavior_name, actions)

    env.step()

    decision_steps, terminal_steps = env.get_steps(behavior_name)
    next_states = decision_steps.obs[0]
    rewards = decision_steps.reward
    done = terminal_steps.obs[0]

    # save experience to replay buffer
    agent.replay_buffer.save(states, actions, rewards, next_states)

    # reset the environment if terminal step was reached
    if len(done) > 0 or (episode_length == MAX_EPISODE_LEN):
        env.reset()
        decision_steps, terminal_steps = env.get_steps(behavior_name)
        episode_length = 0
        total_rewards.append(np.mean(episode_reward))
        print(f'episode reward: {episode_reward}')
        episode_reward = np.zeros(10)

    # fill buffer before updating the agent
    if t > WARMUP_PHASE:
        agent.update()

    # update observation
    states = next_states

    #print(f'{t} : {np.mean(rewards)}')

    episode_reward += rewards
    episode_length += 1

    # save model every 100 timesteps
    if (t % 100) == 0:
        agent.save_model(NAME)

env.close()
plt.plot(total_rewards)
plt.xlabel('Episode')
plt.ylabel('Average reward')
plt.savefig(NAME + '.png')
