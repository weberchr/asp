import rooms
import dqn as abc
import matplotlib.pyplot as plot
import gym
import numpy as np
import operator
from itertools import product

def episode(env, agent, nr_episode=0):
    state = env.reset()
    undiscounted_return = 0
    discount_factor = 0.99
    done = False
    time_step = 0
    while not done:
        env.render()
        # 1. Select action according to policy
        action = agent.policy(state)
        # 2. Execute selected action
        next_state, reward, done, _ = env.step(action)
        # 3. Integrate new experience into agent
        agent.update(state, action, reward, next_state, done)
        state = next_state
        undiscounted_return += reward
        time_step += 1
    print(nr_episode, ":", undiscounted_return)
    return undiscounted_return







# Domain setup


env1 = 'MountainCar-v0'
env2 = 'Acrobot-v1'
env3 = 'CartPole-v1'

envs = [env1, env2, env3]

bestoverall = {}

for envy in envs:
    env = gym.make(envy)
    params = {}
    params["nr_actions"] = env.action_space.n
    params["nr_input_features"] = env.observation_space.shape[0]
    params["env"] = env

    #Hyperparameters
    gamma = [i for i in np.arange(0.1, 1, 0.1)]
    gamma.append(0.99)
    alpha = [0.1, 0.01, 0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.0001]
    epsilon = [i for i in np.arange(0.1, 1, 0.1)]
    memory_capacity = [i for i in np.arange(4500, 5500, 100)]
    warmup_phase = [i for i in np.arange(500, 1500, 100)]
    target_update_interval = [i for i in np.arange(500, 1500, 100)]
    minibatch_size = [32]
    epsilon_linear_decay = [i for i in np.arange(0.1, 2, 0.1)]
    epsilon_min = [i for i in np.arange(0.01, 0.09, 0.1)]



    training_episodes = 800

    bestresults = {}

    for g, a, e, m, w, t, mi, el, em in product(*[gamma, alpha, epsilon, memory_capacity,
                                                  warmup_phase, target_update_interval,
                                                  minibatch_size, epsilon_linear_decay, epsilon_min]):
        params["gamma"] = g
        params["alpha"] = a
        params["epsilon"] = e
        params["memory_capacity"] = m
        params["warmup_phase"] = w
        params["target_update_interval"] = t
        params["minibatch_size"] = mi
        params["epsilon_linear_decay"] = el / params["memory_capacity"]
        params["epsilon_min"] = em

        # Agent setup
        agent = abc.DQNLearner(params)
        returns = [episode(env, agent, i) for i in range(training_episodes)]

        x = range(training_episodes)
        y = returns


        avg = sum(y[-100:]) / len(y[-100:])
        bestresults[str(params)] = avg

        plot.plot(x, y)
        plot.title("Progress")
        plot.xlabel("episode")
        plot.ylabel("undiscounted return")
        plot.show()

    b = max(bestresults.items(), key=operator.itemgetter(1))[0]
    bestoverall = bestresults[b]

    with open('output.txt', 'a') as f:

        s = str(envy) + ' - ' + str(bestoverall) + ' - ' + str(b) + '\n'
        print(s)
        f.write(s)