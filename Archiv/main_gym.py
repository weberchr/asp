import json
import os
import gym
import numpy as np
import matplotlib.pyplot as plt
from td3 import TD3Agent



'''
was used to test the agent on simple gym environment - now outdated
'''


env = gym.make('Pendulum-v0')

def plot_reward(rewards, filepath):
    """
    Plot and save the reward over episodes.
    """
    plt.rcParams['figure.figsize'] = (30, 15)

    plt.plot(rewards, c='b', linewidth=.6, label='reward')


    plt.xlabel('Episode')
    plt.ylabel('Reward')
    plt.legend(loc='upper left')
    plt.grid(linewidth=.5)
    plt.tight_layout()
    plt.savefig(filepath + 'reward.png')
    plt.close()

def main(trial=None):
    states, episode_reward, episode_length, done = env.reset(), 0, 0, False

    params = {}
    params['num_states'] = env.observation_space.shape[0]
    params['num_actions'] = env.action_space.shape[0]
    params['seed'] = SEED
    params['action_max'] = env.action_space.high[0]
    params['num_agents'] = 1

    if trial is None:
        param_dict = json.load(open("settings/params.json", "r"))
        for p in param_dict:
            params[p] = param_dict[p]

    agent = TD3Agent(params)

    total_steps = NUM_EPISODES * MAX_EPISODE_LEN

    total_rewards = []
    episode_length = 0
    episode_reward = 0

    for t in range(total_steps):
        if t > WARMUP_PHASE:
            actions = agent.policy(np.array([states]))[0]
            actions += 0.1 * np.random.randn()
            actions = np.clip(actions, -1, 1)
        else:
            actions = env.action_space.sample()

        # send actions to the environment, make one step
        next_states, rewards, done, _ = env.step(actions)

        episode_reward += rewards
        episode_length += 1

        # save experience to replay buffer
        agent.replay_buffer.save([states], [actions], [rewards], [next_states])

        # reset the environment if terminal step was reached
        if done:
            total_rewards.append(episode_reward)
            print(f'{t + 1} - {episode_length} : {episode_reward}')
            plot_reward(total_rewards, MODEL_PATH)
            states, episode_reward, episode_length, done = env.reset(), 0, 0, False
            agent.save_model(MODEL_PATH)
            continue

        # update the agent as soon as the buffer is filled
        if t > 1000:
            agent.update(t)

        # update observation
        states = next_states

        env.render()


if __name__ == '__main__':
    SEED = 42
    NUM_EPISODES = 400
    MAX_EPISODE_LEN = 1000
    WARMUP_PHASE = 10000

    NAME = 'gym_test'
    MODEL_PATH = f'Modelle/{NAME}/'

    os.makedirs(MODEL_PATH, exist_ok=True)

    main()

    env.close()
